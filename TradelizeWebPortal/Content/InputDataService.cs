﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradelizeWebPortal.Objects;
using TradelizeWebPortal.Pages;
using TradelizeWebPortal.Properties;
using OpenQA.Selenium;
using System.Xml.Serialization;
using System.IO;

namespace TradelizeWebPortal.Content
{
    public class InputDataService : Base
    {
        public InputDataService(IWebDriver driver) : base(driver)
        {

        }

        public List<User> GetAllUsersFromXml()
        {
            XmlSerializer reader = new XmlSerializer(typeof(List<User>));
            TextReader file = new StringReader(Resources.users);
            var list = (List<User>)reader.Deserialize(file);
            file.Close();
            return list;
        }

        public List<string> GetAccessList()
        {
            XmlSerializer reader = new XmlSerializer(typeof(List<string>));
            TextReader file = new StringReader(Resources.AccessList);
            var list = (List<string>)reader.Deserialize(file);
            file.Close();
            return list;
        }

        public List<InccorectInputData> GetTestInputData(string root)
        {
            XmlSerializer reader = new XmlSerializer(typeof(List<InccorectInputData>));
            TextReader file = new StringReader(root);
            var list = (List<InccorectInputData>)reader.Deserialize(file);
            file.Close();
            return list;
        }

        public User GetMainUser(string server)
        {
            var users = GetAllUsersFromXml();
            var user = users.FirstOrDefault(x => x.Role == "Main" && x.Server == server);
            return user;
        }

        public IEnumerable<User> GetTestUsers(string server)
        {
            var users = GetAllUsersFromXml();
            var sortUserByServer = from u in users
                               where u.Role != "Main" && u.Server == server
                               select u;
            return sortUserByServer;
        }
    }
}
