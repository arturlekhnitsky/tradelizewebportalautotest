﻿using OpenQA.Selenium;
using System.Collections.Generic;

namespace TradelizeWebPortal.Pages
{
    public class SocialFeed : Base
    {
        public SocialFeed(IWebDriver driver) : base(driver)
        {

        }

       
        By commentInput = By.CssSelector("");
        By feedContentBlock = By.Id("content");

        public (string actualTabName,int actualTabCount) GetActualTabsOnSocialFeed()
        {
            string tabs = "";
            IWebElement webElement = driver.FindElement(By.ClassName("tab-wrapper"));
            IList<IWebElement> allSpan = webElement.FindElements(By.TagName("span"));
            int RowCount = allSpan.Count;

            foreach (var item in allSpan)
            {
                tabs = tabs + item.Text + "; ";
            }

            return (tabs,RowCount);
        }

        public bool IsSocialFeedOpen()
        {
            return IsElementPresent(feedContentBlock);
        }
        
}
}
