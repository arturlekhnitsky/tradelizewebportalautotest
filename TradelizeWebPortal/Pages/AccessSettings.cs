﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradelizeWebPortal.Content;

namespace TradelizeWebPortal.Pages
{
    public class AccessSettings : InputDataService
    {

        public AccessSettings(IWebDriver driver) : base(driver)
        {

        }

        By stats = By.Id("PROFILE_TAB_STATS-link");
        By diary = By.Id("PROFILE_TAB_DIARY-link");
        By trades = By.XPath("//span[contains(text(),'TRADES')]");
        By statisticLevel = By.CssSelector("select[formcontrolname='statsAccessLevel']");
        By tradeLevel = By.CssSelector("select[formcontrolname='tradesAccessLevel']");

        //Trader Profile
        By traderTrades = By.CssSelector("span[class='name']"); //Tab DIARY => Trades
        By traderStatistic = By.Id("PROFILE_TAB_STATS-link"); //Stats Tab
        By traderName = By.ClassName("username");


        By titleProfileSettings = By.XPath("//legend[contains(text(),'PROFILE SETTINGS')]"); //title Profile Settings on the Settings Page



        public bool GetExpectedAccess(string role, string access)
        {
            string r = role;
            string acc = access;

            if (acc == "ALL_PUBLIC")
            {
                return true;
            }
            if (acc == "ALL" && r != "Guest")
            {
                return true;
            }
            if (acc == "MIRRORING_FOLLOWERS" && (r == "Mirroring" || r == "Paid"))
            {
                return true;
            }
            if (acc == "CHANNEL_FOLLOWERS" && (r == "Channel" || r == "Paid"))
            {
                return true;
            }
            if (acc == "PAID_FOLLOWERS" && (r == "Channel" || r == "Mirroring" || r == "Paid"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        
        public void GoToTraderProfile(string trader, string serverName)
        {
            switch (serverName)
            {
                case "QA":
                    GoUrl($"https://qa-io.tradelize.com/portal/trader/profile/{trader}");
                    WaitUntilElementPresent(traderName, 10);
                    break;
                case "PROD":
                    GoUrl($"https://tradelize.com/portal/trader/profile/{trader}");
                    WaitUntilElementPresent(traderName, 10);
                    break;
                default:
                    GoUrl($"https://qa-io.tradelize.com/portal/trader/profile/{trader}");
                    WaitUntilElementPresent(traderName, 10);
                    break;
            }
            
        }

        public void SelectTradeAccess(string valueLevel)
        {
            string value = valueLevel;
            //select the drop down list
            var level = driver.FindElement(tradeLevel);
            //create select element object
            var selectElement = new SelectElement(level);

            //select by value
            selectElement.SelectByValue(value);
        }

        public void SelectStatisticAccess(string valueLevel)
        {
            string value = valueLevel;
            //select the drop down list
            var level = driver.FindElement(statisticLevel);
            //create object of the selected element 
            var selectElement = new SelectElement(level);

            //select by value
            selectElement.SelectByValue(value);
        }

        public void ChangeTradeAccess(string server, string accessName)
        {
            OpenSettingsPage(server);
            SelectTradeAccess(accessName);
        }

        public void ChangeStatisticAccess(string server, string accessName)
        {
            OpenSettingsPage(server);
            SelectStatisticAccess(accessName);
        }

        public void OpenSettingsPage(string serverName)
        {
            switch (serverName)
            {
                case "QA":
                    GoUrl("https://qa-io.tradelize.com/portal/settings");
                    WaitUntilElementPresent(titleProfileSettings, 10);
                    break;
                
                case "PROD":
                    GoUrl("https://tradelize.com/portal/settings");
                    WaitUntilElementPresent(titleProfileSettings, 10);
                    break;
                
                default:
                    GoUrl("https://qa-io.tradelize.com/portal/settings");
                    WaitUntilElementPresent(titleProfileSettings, 10);
                    break;
            }

        }

        public void GoToTradesInDiary()
        {
            Click(diary);

            Click(trades);
        }

        public bool AreTradesInDiaryPresent()
        {
            return IsElementPresent(traderTrades);
        }

        public bool IsStatisticPresent()
        {
            return IsElementPresent(traderStatistic);
        }

    }
}
