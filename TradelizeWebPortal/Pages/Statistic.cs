﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradelizeWebPortal.Pages
{
    public class Statistic : Base
    {
        public Statistic(IWebDriver driver) : base(driver)
        {

        }

        By statisticBlock = By.Id("statistic");

        public bool IsStatisticPageOpen()
        {
            try
            {
                WaitUntilElementPresent(statisticBlock, 3);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
           
           
        }


    }
}
