﻿using OpenQA.Selenium;

namespace TradelizeWebPortal.Pages
{
    public class MyProfile : Base
    {
        public MyProfile(IWebDriver driver) : base(driver)
        {

        }

        By changeAvatarBtn = By.ClassName("change-avatar");
        By changeBackgroundImgBtn = By.ClassName("change-background");
        By uploadModal = By.ClassName("crop-modal");
        By uploadImgBtn = By.ClassName("btn-upload");
        By idProfile = By.Id("profile");

        public bool IsProfileBlockPresent()
        {
            WaitUntilElementPresent(idProfile, 10);
            return IsElementPresent(idProfile);
        }
    }
}
