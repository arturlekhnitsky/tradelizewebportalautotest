﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using TradelizeWebPortal.Objects;
using TradelizeWebPortal.Properties;

namespace TradelizeWebPortal.Pages
{
    public class Registration : Settings
    {

        public Registration(IWebDriver driver) : base(driver)
        {

        }


        #region elements
        By userRegLogin = By.CssSelector("input[formcontrolname='login']");
        By userRegEmail = By.CssSelector("input[formcontrolname='email']");
        By retypeEmail = By.CssSelector("input[formcontrolname='re-email']");
        By userRegPass = By.CssSelector("input[formcontrolname='password']");
        By retypePass = By.CssSelector("input[formcontrolname='re-password']");
        By RefferalCode = By.CssSelector("input[formcontrolname='referral']");
        By checkBoxAgreement = By.CssSelector("label[class='custom-checkbox']");
        By signUpbtn = By.ClassName("welcome-btn");
        By agreementLink = By.LinkText("AGREEMENT");

        //page with instruction
        By instructionPageOk = By.ClassName("ok");
        #endregion


        public void OpenRegPage(string serverName)
        {

            switch (serverName)
            {
                case "QA":
                    GoUrl("https://qa-io.tradelize.com/portal/auth/signup");
                    WaitUntilElementPresent(By.ClassName("logo"), 10);
                    break;

                case "PROD":
                    GoUrl("https://tradelize.com/portal/auth/signup");
                    WaitUntilElementPresent(By.ClassName("logo"), 10);
                    break;

                case "QA-RELEASE":
                    GoUrl("https://qa-release-portal.tradelize.com/auth/signup");
                    WaitUntilElementPresent(By.ClassName("logo"), 10);
                    break;

                default:
                    GoUrl("https://qa-io.tradelize.com/portal/auth/signup");
                    WaitUntilElementPresent(By.ClassName("logo"), 10);
                    break;
            }

            MaximizeWindow();

        }

        public string GetRefCode(string serverName)
        {
            string refCode;

            switch (serverName)
            {
                case "QA":
                    refCode = "7faf1110-64df-458d-86f3-79f4c53c88a6";
                    break;
                case "PROD":
                    refCode = "e2edb57a-6dec-41d2-a992-486d48d19de4";
                    break;
                case "QA-RELEASE":
                    refCode = "6f7f627b-eeaf-41b4-a8f5-f2a7c118473c";
                    break;

                default:
                    refCode = "7faf1110-64df-458d-86f3-79f4c53c88a6";
                    break;
            }
            return refCode;
        }

        public void InputSignUpData(string refCode)
        {
            string password = "Abcde12345";
            string mailLogin = GenerateMail();
            Console.WriteLine("Registration login = " + mailLogin);
            Console.WriteLine($"Password = {password}");


            using (StreamWriter file1 = new StreamWriter(@"D:\NewRegUsers.txt"))
            {
                file1.WriteLine(mailLogin);
            }

            WriteText(userRegLogin, mailLogin.Substring(0, mailLogin.IndexOf('@')));
            WriteText(userRegEmail, mailLogin);
            WriteText(userRegPass, password);
            WriteText(retypePass, password);
            WriteText(RefferalCode, refCode);
            Click(checkBoxAgreement);

        }

        public void InputSignUpDataWithOutRefCode()
        {
            string password = "Abcde12345";
            string mailLogin = GenerateMail();
            Console.WriteLine("Registration login = " + mailLogin);
            Console.WriteLine($"Password = {password}");

            using (StreamWriter file1 = new StreamWriter(@"D:\NewRegUsers.txt"))
            {
                file1.WriteLine(mailLogin);
            }

            WriteText(userRegLogin, mailLogin.Substring(0, mailLogin.IndexOf('@')));
            WriteText(userRegEmail, mailLogin);
            WriteText(userRegPass, password);
            WriteText(retypePass, password);
            Click(checkBoxAgreement);
        }

        public void RegistrationActions(string server, bool inviteCodeFlag)
        {
            OpenRegPage(server);
            if (inviteCodeFlag)
            {
                InputSignUpData(GetRefCode(server));
            }
            else
            {
                InputSignUpDataWithOutRefCode();
            }
            ClickRegButton();
        }

        public void ClickAgreementLinkPdf()
        {
            driver.FindElement(agreementLink).Click();
        }

        public void ClickRegButton()
        {
            try
            {
                Click(signUpbtn);
                WaitUntilElementPresent(instructionPageOk, 10);
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine("Page with instruction is not present");
                throw;
            }
            
        }

        public bool RegistrationIsSuccess()
        {
            string expected = "?code=SUCCESS_ALERT_ACTIVATED";
            string Url = GetCurrentUrl();
            string codFromUrl = Url.Substring(Url.IndexOf("?"));

            if (codFromUrl == expected)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public IEnumerable<InccorectInputData> GetDataForLoginBox()
        {
            List<InccorectInputData> inputs = GetTestInputData(Resources.inputs);

            var loginBox = from lbox in inputs
                           where lbox.InputBoxName == "Login"
                           select lbox;

            return loginBox;
        }

        public IEnumerable<InccorectInputData> GetDataForEmailBox()
        {
            List<InccorectInputData> inputs = GetTestInputData(Resources.inputs);

            var emailBox = from ebox in inputs
                           where ebox.InputBoxName == "Email"
                           select ebox;

            return emailBox;
        }

        public IEnumerable<InccorectInputData> GetDataForPasswordBox()
        {
            List<InccorectInputData> inputs = GetTestInputData(Resources.inputs);

            var passBox = from pbox in inputs
                          where pbox.InputBoxName == "Password"
                          select pbox;

            return passBox;
        }

        public IEnumerable<InccorectInputData> GetDataForRePasswordBox()
        {
            List<InccorectInputData> inputs = GetTestInputData(Resources.inputs);

            var repassBox = from rebox in inputs
                            where rebox.InputBoxName == "Re-Password"
                            select rebox;

            return repassBox;
        }

        public string GetBorderColor(string byElement)
        {
            IWebElement loginInputBox = driver.FindElement(By.CssSelector(byElement));
            string colorCode = loginInputBox.GetCssValue("border-top-color");
            return colorCode;
        }
    }
}
