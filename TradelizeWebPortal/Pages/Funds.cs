﻿using OpenQA.Selenium;

namespace TradelizeWebPortal.Pages
{
    public class Funds : Base
    {
        public Funds(IWebDriver driver) : base (driver)
        {

        }

        #region elements
        //[API]Exchange TAB
        By navTabs = By.ClassName("nav-tabs");
        By navItem = By.ClassName("nav-item");
        By filterMenu = By.ClassName("filter-menu");
        By apiKeyTable = By.TagName("app-funds-exchanges-api-keys");
        By fundsTable = By.TagName("app-funds-exchanges-balance");
        By fundsNavMenu = By.XPath("//a[contains(text(),'FUNDS')]");
        By tradelizeExchange = By.XPath("//app-funds-exchanges-api-keys//td[contains(text(),'TRADELIZE')]");
        By valid = By.XPath("//td[contains(text(),'VALID')]");

        //Tradelize Wallet TAB
        By tradelizeWallet = By.XPath("//span[contains(text(),'TRADELIZE WALLET')]");
        By depositBtn = By.XPath("//button[contains(text(),'DEPOSIT')]");
        By modalDepositWindow = By.XPath("//h4[contains(text(),'DEPOSIT')]");
        By walletAddress = By.ClassName("addr");

        By exchangeBlock = By.ClassName("exchange-block");
        #endregion

        public void OpenFundsPage()
        {
            Click(fundsNavMenu);
            WaitUntilElementPresent(tradelizeExchange, 10);
        }

        public void OpenTradelizeWalletTab()
        {
            Click(tradelizeWallet);
            WaitUntilElementPresent(depositBtn, 5);
        }

        public void ClickDeposit()
        {
            Click(depositBtn);
            WaitUntilElementPresent(modalDepositWindow, 5);
        }

        public string GetWalletAddress()
        {
            return GetText(walletAddress);
        }

        public bool IsDemoExchangeCreate()
        {
            string expectedExch = "TRADELIZE";
            string expectedIsValid = "VALID";
            string actualExchange = driver.FindElement(tradelizeExchange).Text;
            string actualIsValid = driver.FindElement(valid).Text;

            if (expectedExch == actualExchange && expectedIsValid == actualIsValid)
                return true;
            else
                return false;
            
        }

        public bool IsFundsPageOpen()
        {
            return IsElementPresent(exchangeBlock);
        }
     
    }
}
