﻿using OpenQA.Selenium;
using System;

namespace TradelizeWebPortal.Pages
{
    public class Mirroring : Base
    {
        public Mirroring(IWebDriver driver) : base(driver)
        {

        }

        #region elements
        By editCurrentPriceBtn = By.CssSelector("button[class='btn edit']");
        By costOn = By.CssSelector("option[value='true']");
        By costOff = By.CssSelector("option[value='false']");
        By updateBtn = By.XPath("//span[contains(text(),'Update')]"); // найти получше селектор
        By updateUpcomingOffersBtn = By.CssSelector("span[class='ng-star-inserted']");
        By addOfferBtn = By.ClassName("btn-add-offer");
        By editUpcomingPriceBtn = By.XPath("//td[7]//button[1]");  //найти получше селектор
        By modalWindow = By.ClassName("modal-content");
        By optionOn = By.XPath("//option[contains(text(),'ON')]");
        By creatNewOffer = By.ClassName("btn-add-offer");
        By statusDrpBox = By.CssSelector("select[formcontrolname='isActive']");
        By checkingUpdate = By.XPath("//*[@class='svg-inline--fa fa-check fa-w-16']");

        //on the profile page
        By costFree = By.XPath("//table[@class='subscribe-table align-right']//tbody//tr//td//i");
        By copyMode = By.XPath("//label[contains(text(),'COPY ONLY NEW POSITIONS')]"); //Only new positions
        By inputStopCopyWhen = By.XPath("//input[@formcontrolname='stopCopyWhen']");
        By iAgree = By.XPath("//label[@class='custom-checkbox ng-star-inserted']");
        By iAgree2 = By.XPath("//label[@class='custom-checkbox']");
        By startBtn = By.XPath("//span[@class='ng-star-inserted'][contains(text(),'START')]");
        By startBtn2 = By.XPath("//span[contains(text(),'START')]");
        #endregion

        public void OpenMirroringPage()
        {
            driver.Navigate().GoToUrl("https://qa-io.tradelize.com/portal/create-mirror");
        }
        
        public void StartMirroring()
        {
            WriteText(inputStopCopyWhen, "0");
            WaitUntilElementPresent(costFree, 5);
            Click(costFree);
           
            if (IsElementPresent(iAgree))
                Click(iAgree);
            else
                Click(iAgree2);

            Click(copyMode);


            if (IsElementPresent(startBtn))
            {
                WaitUntilElementPresent(startBtn, 5);
                Click(startBtn);
            }
            else
            {
                WaitUntilElementPresent(startBtn2, 5);
                Click(startBtn2);
            }
        }

        public void SelectStatusON()
        {
            WaitUntilElementPresent(editCurrentPriceBtn, 10);
            Click(editCurrentPriceBtn);
            WaitUntilElementPresent(optionOn, 10);
            Click(optionOn);
            WaitUntilElementPresent(editCurrentPriceBtn, 10);
            Click(editCurrentPriceBtn);
            WaitUntilElementPresent(updateBtn, 10);
            Click(updateBtn);
        }

        public bool CurrentOffersON()
        {

            try
            {
                bool presentCondition = IsElementPresent(editCurrentPriceBtn);
                //bool isElementDisplayed = driver.FindElement(editCurrentPriceBtn).Displayed;

                if (presentCondition)
                {

                    driver.FindElement(editCurrentPriceBtn).Click();
                    driver.FindElement(costOn).Click();
                    driver.FindElement(editCurrentPriceBtn).Click();
                    driver.FindElement(updateBtn).Click();
                    
                    return true;
                }

                else
                {
                    driver.FindElement(addOfferBtn).Click();
                    driver.FindElement(updateBtn);

                    if (IsElementPresent(modalWindow))
                    {
                        Console.WriteLine("At the moment you are Mirrored user");
                        return false;
                    }

                    else
                    {
                        driver.FindElement(updateBtn).Click();
                        return true;
                    }
                    
                 }
            }

            catch (Exception e)
            {
                Console.WriteLine(e);
                return false;
            }

        }

        public void CreateNewPlan()
        {
            //Click(creatNewOffer);
            //Click(editUpcomingPriceBtn);
            //WaitUntilElementPresent(statusDrpBox, 10);
            //Click(statusDrpBox);
            //Click(costOn);
            //Click(checkingUpdate);
            //Click(updateUpcomingOffersBtn);
            Console.WriteLine("no created offers! Please create one!");

        }
                              
        
}
}
