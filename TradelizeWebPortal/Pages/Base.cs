﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using TradelizeWebPortal.Properties;

namespace TradelizeWebPortal.Pages
{
    public class Base
    {
        protected IWebDriver driver;


        public Base(IWebDriver driver)
        {
            this.driver = driver;
        }

        public void WriteText(By element, string text)
        {
            driver.FindElement(element).SendKeys(text);
        }

        public void Click(By element)
        {
            driver.FindElement(element).Click();
        }

        public string GetText(By element)
        {
            try
            {
                string actual = driver.FindElement(element).Text;
                return actual;
            }
            catch (NoSuchElementException e)
            {
                return e.ToString();
                throw;
            }
           
        }

        public string GetCurrentUrl()
        {
            string currentUrl = driver.Url;
            return currentUrl;
        }

        public void Sleep(int sec)
        {
            sec *= 1000;
            Thread.Sleep(sec);
        }

        public void GoUrl(string url)
        {
            driver.Navigate().GoToUrl(url);
        }

        public bool IsFileExists(string name)
        {
            string currentFile = @"C:\Users\User\Downloads\" + name + "";
            if (File.Exists(currentFile))
                return true;
            else
                return false;
        }

        public void DeleteFileFromDirectory(string fileAdress)
        {

            if (File.Exists(fileAdress))
            {
                File.Delete(fileAdress);
            }

        }

        public bool CompareHashes(string expectedHash, string actualHash)
        {
            if (expectedHash == actualHash)

                return true;

            else
                return false;
        }

        public string GetHeshSum(string fName)
        {
            using (var md5 = MD5.Create())
            {
                using (var stream = File.OpenRead(fName))
                {
                    var hash = md5.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", "").ToUpperInvariant();
                }
            }
        }

        public void CheckFileIsDownload(string fName, string dPath)
        {

            string downloadPath = dPath;
            string fileName = fName;
            string pathFileName = downloadPath + "\\" + fileName;

            for (int i = 0; i < 30; i++)
            {
                if (File.Exists(pathFileName))
                {
                    break;
                }
                Sleep(1);
            }

            var lenght = new FileInfo(pathFileName).Length;
            for (int i = 0; i < 30; i++)
            {
                Sleep(1);
                var newLength = new FileInfo(pathFileName).Length;
                if (newLength == lenght && lenght != 0)
                {
                    break;
                }
                lenght = newLength;
            }
        }


        public string GenerateMail()
        {
            int nmb = NumberIncrement();
            string MailService = "@yopmail.com";
            string Login = "MyLuckyNmb" + nmb;
            string Email = Login + MailService;
            return Email;
        }

        public int NumberIncrement()
        {
            int nmb = GetNumFromCountTxt() + 1;
            SetNumToCountTxt(nmb);
            return nmb;
        }

        public int GetNumFromCountTxt()
        {
            int num;
            using (StreamReader file1 = new StreamReader(@"D:\Count.txt"))
            {
               num = int.Parse(file1.ReadLine());
            }

            return num;
        }

        public void SetNumToCountTxt(int n)
        {
            string num = n.ToString();
            using (StreamWriter file1 = new StreamWriter(@"D:\Count.txt"))
            {
                file1.WriteLine(num);
            }
        }


        public void WriteToTxtFile()
        {
            using (StreamWriter file1 = new StreamWriter(Resources.Count))
            {
                file1.WriteLine();
            }
        }
        public void CreateNewTab()
        {
            IWebElement body = driver.FindElement(By.TagName("body"));
            Thread.Sleep(5000);
            body.SendKeys(Keys.Control + "T");
        }

        public void SwitchTabToLast()
        {
            driver.SwitchTo().Window(driver.WindowHandles.Last());
        }

        public void SwitchTabToFirst()
        {
            driver.SwitchTo().Window(driver.WindowHandles.First());
        }

        public void WaitUntilElementPresent(By element, int sec)
        {
            try
            {
                new OpenQA.Selenium.Support.UI.WebDriverWait(driver, TimeSpan.FromSeconds(sec)).
                Until(ExpectedConditions.ElementToBeClickable(element));
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
                //throw;
            }

        }

        public bool IsElementPresent(By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                Console.WriteLine($"The Element {by} is not present");
                return false;
            }

        }

        public string GetTitle()
        {
            string title = driver.Title;
            return title;
        }

        public void MaximizeWindow()
        {
            driver.Manage().Window.Maximize();
        }

        public void Clear(By element)
        {
            driver.FindElement(element).Clear();
        }

        public void RefreshPage()
        {
            driver.Navigate().Refresh();
        }

        public void SwitchToActiveElement()
        {
            driver.SwitchTo().ActiveElement();
        }
    }
}
