﻿using OpenQA.Selenium;

namespace TradelizeWebPortal.Pages
{
    public class GetTerminal : Base
    {
        private string url = "https://tradelizesoftware.com/";

        public GetTerminal(IWebDriver driver) : base(driver)
        {

        }

        By getTerminalBtn = By.LinkText("GET TERMINAL");

        public void OpenDownloadTerminalPage()
        {
            driver.Navigate().GoToUrl(url);
        }

        public void ClickToDownloadTerminal()
        {
            driver.FindElement(getTerminalBtn).Click();
        }




    }
}
