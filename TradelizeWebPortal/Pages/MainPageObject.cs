﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TradelizeWebPortal.Content;

namespace TradelizeWebPortal.Pages
{
    public class MainPageObject : InputDataService
    {
        public MainPageObject(IWebDriver driver) : base(driver)
        {

        }

        // Header menu
        public By modeSwitcher = By.Id("portal-mode-full");
        public By supportBtn = By.LinkText("SUPPORT");


        //dropDown Menu
        public By headerDropDownMenu = By.XPath("//li[@class='dropdown']");
        public By logout = By.XPath("//ul[@class='user-menu']//a[contains(text(),'LOGOUT')]");

        //Demo mode On
        By demoActive = By.ClassName("demo-mode");

        //Side Menu
        By scoreLink = By.LinkText("SCORE");
        By socialFeedLink = By.LinkText("SOCIAL FEED");
        By myProfile = By.LinkText("MY PROFILE");
        By portfolio = By.CssSelector("a[href ='/portal/portfolio']");
        By statistic = By.CssSelector("a[href ='/portal/statistic']");

        //ScoreList
        By scoreContent = By.ClassName("tab-content");

        public bool IsHeaderDropDownNenuPresent()
        {
            return IsElementPresent(headerDropDownMenu);
        }

        public void ClickSwitchMode()
        {
            Click(modeSwitcher);
        }

        public bool IsDemoModeActive()
        {
            WaitUntilElementPresent(demoActive, 3);
            return IsElementPresent(demoActive);
        }

        public void OpenTradelizePortal(string server)
        {
            try
            {
                switch (server)
                {
                    case "QA":
                        GoUrl("https://qa-io.tradelize.com/portal/");
                        MaximizeWindow();
                        WaitUntilScoreListPresent();
                        break;
                    case "PROD":
                        GoUrl("https://tradelize.com/portal/score");
                        MaximizeWindow();
                        WaitUntilScoreListPresent();
                        break;
                    default:
                        GoUrl("https://qa-io.tradelize.com/portal/");
                        MaximizeWindow();
                        WaitUntilScoreListPresent();
                        break;
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.StackTrace);
            }
        }

        public void WaitUntilScoreListPresent()
        {
            WaitUntilElementPresent(scoreContent, 10);
            Sleep(4);
        }

        // Open Score Page
        public void OpenScorePage()
        {
            driver.FindElement(scoreLink).Click();
        }

        //Open My Profile

        //Open Social Feed
        public void OpenSocialFeedPage()
        {
            Click(socialFeedLink);
        }

        public void OpenSupportPage()
        {
            Click(supportBtn);
            WaitUntilElementPresent(By.ClassName("tickets-table-block"), 10);
            
        }

        public void OpenMyProfilePage()
        {
            Click(myProfile);
        }

        public void OpenPorfolioPage()
        {
            Click(portfolio);
        }

        public void OpenStatisticPage()
        {
            Click(statistic);
        }

        
    }
}
