﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradelizeWebPortal.Pages
{
    public class Portfolio : Base
    {
        public Portfolio(IWebDriver driver) : base(driver)
        {

        }

        By portfolioBlock = By.Id("portfolio");

        public bool IsPortfolioPageOpen()
        {
            try
            {
                WaitUntilElementPresent(portfolioBlock, 10);
                return IsElementPresent(portfolioBlock);
            }
            catch (Exception)
            {
                return false;
            }
            
        }
    }
}
