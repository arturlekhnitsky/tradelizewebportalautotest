﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradelizeWebPortal.Pages
{
    public class Settings : MainPageObject
    {
        public Settings(IWebDriver driver) : base (driver)
        {

        }


        By titleProfileSettings = By.XPath("//legend[contains(text(),'PROFILE SETTINGS')]");


        public void WaitProffileSettingsPresent()
        {
            WaitUntilElementPresent(titleProfileSettings, 10);
        }

        
    }
}
