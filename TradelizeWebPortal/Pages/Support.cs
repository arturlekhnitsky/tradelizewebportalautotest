﻿using OpenQA.Selenium;
using SeleniumExtras.WaitHelpers;
using OpenQA.Selenium.Support.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradelizeWebPortal.Pages
{
    public class Support : Base
    {
        public Support(IWebDriver driver) : base(driver)
        {

        }

        By createTicketBtn = By.ClassName("create-ticket-btn");
        By inputSubject = By.ClassName("subject");
        By inputMessage = By.ClassName("message ");
        By dropDownPlatform = By.Id("dropdownPlatform");
        By dropdownCategory = By.Id("dropdownCategory");
        

        public void CreateTicket()
        {

        }

        public void SelectPlatform()
        {

        }

        public void SelectCategory()
        {

        }

        public void AddAttachments()
        {

        }

        public void TypeSubsject()
        {
            WriteText(inputSubject, GetSubjectName());
        }

        //Generate Subject Name
        public string GetSubjectName()
        {
            Random rndm = new Random();
            int nmb;
            nmb = rndm.Next(1,9999999);
            string subjectName = "TestTicket - " + nmb;
            return subjectName;
        }

        public void TypeMessage(string text)
        {
            WriteText(inputMessage, text);
        }

        public void SelectDropDown(By element, string value)
        {
            Click(element);

            
        }

        public bool IsCreateBtnPresent()
        {
            return IsElementPresent(createTicketBtn);
        }
        
    }
}
