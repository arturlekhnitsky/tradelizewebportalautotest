﻿using OpenQA.Selenium;
using System;

namespace TradelizeWebPortal.Pages
{
    public class Login : Settings
    {
        //TO DO: создать ексельку и тянуть юзеров оттуда
        //string login = "tqav3";

        public Login(IWebDriver driver) : base(driver)
        {

        }


        #region elements
        // Login Page
        By userLogin = By.CssSelector("input[formcontrolname='login']");
        By userPassword = By.CssSelector("input[formcontrolname='password']");
        By welcomeBtn = By.ClassName("welcome-btn");
        By forgotPassword = By.LinkText("FORGOT PASSWORD?");
        By registerNowlink = By.LinkText("OR REGISTER NOW");
        

        //Forgot Password
        By recoveryByEmail = By.CssSelector("input[formcontrolname='email']");
        By recoveryBtn = By.ClassName("welcome-btn");
        By rememberPass = By.LinkText("REMEMBER PASSWORD?");

        //The Score page is open If registration is succesful. Find H1 tag with "TRADALIZE TOP SCORE TRADES" 
        By logoTitle = By.ClassName("logo");


        #endregion

        public void OpenLoginPage(string serverName)
        {

            switch (serverName)
            {
                case "QA":
                    driver.Navigate().GoToUrl("https://qa-io.tradelize.com/portal/auth");
                    driver.Manage().Window.Maximize();
                    break;
               
                case "PROD":
                    driver.Navigate().GoToUrl("https://tradelize.com/portal/auth");
                    driver.Manage().Window.Maximize();
                    break;
                
                default:
                    driver.Navigate().GoToUrl("https://qa-io.tradelize.com/portal/auth");
                    driver.Manage().Window.Maximize();
                    break;
            }

        }

        public void SignInByLogin(string server, string login, string pass)
        {
            if(IsElementPresent(userLogin) == false)
            OpenLoginPage(server);
            
            WriteText(userLogin, login);
            WriteText(userPassword, pass);
            Click(welcomeBtn);
            WaitUntilScoreListPresent();
        }

        public void SignInByEmail(string email, string pass)
        {
            WriteText(userLogin, email);
            WriteText(userPassword, pass);
            Click(welcomeBtn);
        }

        public void ForgotPassword(string email)
        {
            Click(forgotPassword);
            WriteText(recoveryBtn, email);
            Click(recoveryBtn);
        }


        public void ClickToSignIn()
        {
            Click(By.LinkText("SIGN IN"));
        }

        
        public void LogOut()
        {
            Click(headerDropDownMenu);

            Click(logout);
        }

    }
}
