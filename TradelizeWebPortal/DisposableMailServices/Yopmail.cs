﻿using OpenQA.Selenium;
using System;
using System.IO;
using TradelizeWebPortal.Pages;

namespace TradelizeWebPortal.DisposableMailServices
{
    public class Yopmail : Base
    {
        public Yopmail(IWebDriver driver) : base(driver)
        {

        }

        #region elements
        By emailNameYopmail = By.Id("login");
        By sbut = By.ClassName("sbut");
        By letterTradelize = By.LinkText("Confirm email");
        By part = By.PartialLinkText("Confirm email");
        By refreshBtn = By.Id("lrefr");
        By iFrame = By.Id("ifmail");
        #endregion

        public void OpenYopmail()
        {
            driver.Navigate().GoToUrl("http://www.yopmail.com/");
        }

        public string GetUseRegLogin()
        {
            string login;

            using (StreamReader reader = new StreamReader(@"D:\NewRegUsers.txt"))
            {
                login = reader.ReadLine();

            }
            return login;
        }

        public void OpenEmailBoxOnTheYopmail(string login)
        {
            WriteText(emailNameYopmail, login);

            Click(sbut);
        }

        public void FollowByRegLink()
        {

            try
            {
                //driver.Navigate().Refresh();
                Click(refreshBtn);
                Sleep(2);
                //IWebElement iframe = driver.FindElement(By.Id("ifmail"));
                IWebElement iframe = GetiFrame(iFrame);
                Sleep(2);
                driver.SwitchTo().Frame(iframe);
                Sleep(2);
                Click(letterTradelize);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                FollowByRegLink();
            }

        }

        public IWebElement GetiFrame(By element)
        {
            IWebElement iframe = driver.FindElement(element);
            return iframe;
        }


        public void UseActivetedCodeFromEmail()
        {
            OpenYopmail();
            WaitUntilElementPresent(By.Id("login"), 10);
            OpenEmailBoxOnTheYopmail(GetUseRegLogin());
            Sleep(5);
            FollowByRegLink();
        }

    }
}
