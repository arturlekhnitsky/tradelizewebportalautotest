﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradelizeWebPortal.Objects
{
    public class Server
    {
        public string Name { get; set; }
        public string Url { get; set; }
    }
}
