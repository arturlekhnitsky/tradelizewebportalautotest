﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TradelizeWebPortal.Objects
{
    public class InccorectInputData
    {
        public string InputBoxName { get; set; }
        public string TestName { get; set; }
        public string InputData { get; set; }
        public string Element { get; set; }

    }
}
