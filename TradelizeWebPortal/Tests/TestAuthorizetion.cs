﻿using AventStack.ExtentReports;
using NUnit.Framework;
using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TradelizeWebPortal.ExReport;
using TradelizeWebPortal.Objects;
using TradelizeWebPortal.Properties;
using TradelizeWebPortal.Settings;


namespace TradelizeWebPortal.Tests
{

    public class TestAuthorizetion : Browsers
    {

        [Test]
        public void TestAuthorizationByLogin()
        {
            logs = report.CreateTest("AuthorizationByLogin Test");
            var user = accessP.GetMainUser(server);

            loginP.SignInByLogin(server, user.Login, user.Password);
            bool actual = loginP.IsElementPresent(By.ClassName("logo-title"));
            try
            {
                Assert.IsTrue(actual);
                logs.Pass("Test Passed");
            }
            catch (Exception)
            {
                logs.Fail("Failed");
            }
            report.Flush();
        }

        [Test]
        public void DownloadTerminalPCversion()
        {
            getTerm.OpenDownloadTerminalPage();
            getTerm.ClickToDownloadTerminal();

            string downloadPath = "C:\\Users\\User\\Downloads";
            string fileName = "TradelizeLoader.exe";
            string pathFileName = downloadPath + "\\" + fileName;
            string expectedSum = "DD4E51ACFF7E12EE39C4B14B2582D22F";
            getTerm.Sleep(5);
            if (Directory.Exists(downloadPath))
            {
                bool result = getTerm.IsFileExists(fileName);
                if (result == true)
                {
                    string actualSum = getTerm.GetHeshSum(pathFileName);
                    if (getTerm.CompareHashes(expectedSum, actualSum))
                    {
                        getTerm.DeleteFileFromDirectory(pathFileName);
                        Console.WriteLine("The file is successfuly donwload and were deleted");
                    }
                    else
                    {
                        Console.WriteLine($"The actual md5 = {actualSum}, expected MD5 = {expectedSum}");
                        getTerm.DeleteFileFromDirectory(pathFileName);
                        Assert.Fail("The file's md5 is not as expected");
                    }

                }
                else
                {
                    Assert.Fail("The file does not exist");
                }
            }
            else
            {
                Assert.Fail("The directory or folder is not exist");
            }


        }

    }
}
