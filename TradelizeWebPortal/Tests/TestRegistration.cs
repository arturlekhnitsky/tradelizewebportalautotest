﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using System.Linq;
using System.Threading;
using TradelizeWebPortal.Settings;
using AventStack.ExtentReports.Reporter;
using AventStack.ExtentReports;
using TradelizeWebPortal.ExReport;
using System.Collections.Generic;
using TradelizeWebPortal.Properties;
using TradelizeWebPortal.Objects;
using System.Collections;

namespace TradelizeWebPortal.Tests
{
    public class TestRegistration : Browsers
    {
        [Test, Order(5)] // Registration without Invite Code
        public void RegistretionTestWithOutRefCode()
        {
            logs = report.CreateTest("Registration without invite code: " + server);
            bool inviteCode = false;
            string authPageExpected = "PLEASE LOG IN";
            string actualTitle;

            try
            {
                regP.RegistrationActions(server, inviteCode);
                logs.Info("Instruction with next steps send to email.");
                yopP.UseActivetedCodeFromEmail();
                regP.SwitchTabToLast();
                regP.WaitUntilElementPresent(By.TagName("h1"), 10);
                actualTitle = regP.GetText(By.TagName("h1"));

                if (actualTitle == authPageExpected)
                {
                    logs.Info("LOG IN on the portal with login and pass that was used at registration");
                    loginP.SignInByEmail(yopP.GetUseRegLogin(), "Abcde12345");
                    loginP.WaitUntilElementPresent(By.CssSelector("span[class='avatar']"), 5);
                    logs.Info("Check Funds");
                    funds.OpenFundsPage();

                    Assert.IsTrue(funds.IsDemoExchangeCreate(), "Tradelize exchange not found or it is invalid");
                    logs.Info("The wallet Tradelize is created and valid");
                    logs.Pass("Test Registration Passed");
                }
                else
                {
                    logs.Fail("Fail");
                }
            }
            catch (Exception)
            {
                logs.Fail("Fail");
            }

            report.Flush();
        }

        [Test, Order(2)]// Test is registration is succesfuly 
        public void RegistrationTest()
        {
            logs = report.CreateTest("Registration the new user on the server:" + server);
            string authPageExpected = "PLEASE LOG IN";
            bool inviteCode = true;
            string actualTitle;

            try
            {
                regP.RegistrationActions(server, inviteCode);
                logs.Info("Instruction with next steps send to email.");
                yopP.UseActivetedCodeFromEmail();
                regP.SwitchTabToLast();
                regP.WaitUntilElementPresent(By.TagName("h1"), 10);
                actualTitle = regP.GetText(By.TagName("h1"));

                if (actualTitle == authPageExpected)
                {
                    logs.Info("LOG IN on the portal with login and pass that was used at registration");
                    loginP.SignInByEmail(yopP.GetUseRegLogin(), "Abcde12345");
                    regP.WaitUntilElementPresent(By.CssSelector("span[class='avatar']"), 5);
                    logs.Info("Check Funds");
                    funds.OpenFundsPage();
                    try
                    {
                        Assert.IsTrue(funds.IsDemoExchangeCreate(), "Tradelize exchange not found or it is invalid");
                        logs.Info("The wallet Tradelize is created and valid");
                        logs.Log(Status.Pass, "Test Registration Passed");
                    }
                    catch (AssertionException)
                    {
                        logs.Fail("Test Failed");
                    }

                }
                else
                {
                    logs.Fail("Don't find auth page");
                }
            }
            catch (Exception e)
            {
                logs.Info(e.StackTrace);
            }
         
            report.Flush();
        }

        [Test, Order(3)]
        public void IsWalletAddressBtcCreated()
        {
            string actual;
            logs = report.CreateTest("Is BTC wallet address created");
            logs.Info("LOG IN on the portal with login and pass that was used at registration");
            loginP.SignInByLogin(server, yopP.GetUseRegLogin(), "Abcde12345");
            logs.Info("Open Funds page");
            funds.OpenFundsPage();
            funds.OpenTradelizeWalletTab();
            funds.ClickDeposit();
            funds.SwitchToActiveElement();
            actual = funds.GetWalletAddress();
            try
            {
                Assert.IsTrue(actual.Length > 0);
                logs.Info($"wallet adress is: {actual}");
                logs.Pass("Pass");
            }

            catch (AssertionException)
            {
                logs.Info("Address is not created");
                logs.Fail("Fail");

            }

            report.Flush();

        }

        [Test, Order(0)] // Test is agreement present?
        public void VerifyIsAgreementPresent()
        {
            logs = report.CreateTest("Is Agreement Present");
            regP.OpenRegPage(server);
            regP.ClickAgreementLinkPdf();
            Thread.Sleep(2000);
            driver.SwitchTo().Window(driver.WindowHandles.Last());
            string getUrl = regP.GetCurrentUrl();

            try
            {
                Assert.IsTrue(getUrl.Contains(".pdf"));
                logs.Log(Status.Info, "Agreement is present");
                logs.Log(Status.Pass, "Passed");
            }
            catch (AssertionException)
            {

                logs.Log(Status.Fail, "Agreement is not Present");
            }

            report.Flush();
        }

        

        // Test Cases : Test Validation fields of the Registration form
        // Incorrect data

        [Test]
        public void LoginInputBoxValidationTest()
        {
            string expected = "rgba(255, 0, 0, 1)";
            var loginBox = regP.GetDataForLoginBox();

            regP.OpenRegPage(server);

            // Test Login Input Box
            foreach (var i in loginBox)
            {
                logs = report.CreateTest($"{i.InputBoxName} Input Box: -={i.TestName}=-");
                logs.Info($"Input Data: {i.InputData}");
                regP.WriteText(By.CssSelector(i.Element), i.InputData);

                string actualColorCode = regP.GetBorderColor(i.Element);

                try
                {
                    Assert.AreEqual(expected, actualColorCode);
                    logs.Pass("As expected (red border is present)");
                }
                catch (AssertionException)
                {
                    logs.Fail("Red border is not present");
                }
                report.Flush();// Save to Report

                regP.Clear(By.CssSelector(i.Element));
            }

        }

        [Test]
        public void EmailInputBoxValidationTest()
        {
            string expected = "rgba(255, 0, 0, 1)";
            var emailBox = regP.GetDataForEmailBox();

            regP.OpenRegPage(server);
            // Test Email Input Box
            foreach (var i in emailBox)
            {
                logs = report.CreateTest($"{i.InputBoxName} Input Box: -={i.TestName}=-");
                logs.Info($"Input Data: {i.InputData}");
                regP.WriteText(By.CssSelector(i.Element), i.InputData);

                string colorCode = regP.GetBorderColor(i.Element);

                try
                {
                    Assert.AreEqual(expected, colorCode);
                    logs.Pass("As expected (red border is present)");

                }
                catch (AssertionException)
                {
                    logs.Fail("Red border is not present");

                }
                report.Flush();

                regP.Clear(By.CssSelector(i.Element));
            }
        }

        [Test]
        public void PasswordInputBoxValidation()
        {
            string expected = "rgba(255, 0, 0, 1)";
            var passBox = regP.GetDataForPasswordBox();

            regP.OpenRegPage(server);
            // Test Password Input Box
            foreach (var i in passBox)
            {
                logs = report.CreateTest($"{i.InputBoxName} Input Box: -={i.TestName}=-");
                logs.Info($"Input Data: {i.InputData}");
                regP.WriteText(By.CssSelector(i.Element), i.InputData);

                string colorCode = regP.GetBorderColor(i.Element);

                try
                {
                    Assert.AreEqual(expected, colorCode);
                    logs.Pass("As expected (red border is present)");
                }
                catch (AssertionException)
                {
                    logs.Fail("Red border is not present");
                }
                report.Flush();

                regP.Clear(By.CssSelector(i.Element));
            }
        }

        [Test]
        public void RePasswordInputBoxValidation()
        {
            string expected = "rgba(255, 0, 0, 1)";
            var rePassBox = regP.GetDataForRePasswordBox();

            regP.OpenRegPage(server);
            // Test Re-Password Input Box
            foreach (var i in rePassBox)
            {
                logs = report.CreateTest($"{i.InputBoxName} Input Box: -={i.TestName}=-");
                logs.Info($"Input Data(password): {i.InputData}\n Input Data(re-password): {i.InputData}1");

                regP.WriteText(By.CssSelector("input[formcontrolname='password']"), i.InputData);
                regP.WriteText(By.CssSelector(i.Element), i.InputData + "1");

                string colorCode = regP.GetBorderColor(i.Element);

                try
                {
                    Assert.AreEqual(expected, colorCode);
                    logs.Pass("As expected (red border is present)");
                }
                catch (AssertionException)
                {
                    logs.Fail("Red border is not present");
                }

                report.Flush();

                regP.Clear(By.CssSelector(i.Element));
            }
        }

    }


}
