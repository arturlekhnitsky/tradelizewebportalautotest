﻿using NUnit.Framework;
using System;
using TradelizeWebPortal.Settings;
using OpenQA.Selenium;

namespace TradelizeWebPortal.Tests
{
    
    public class TestMirroringModalWindows : Browsers

    {
        [Test]
        public void ActivePriceNoFolowers()//User has created mirroring price and he wants to subscribe to another mirroring.Active price and no folowers
        {            
            string expected = "ATTENTION";
            string login = "case3userB";
            string password = "12345";
            string urlSource = "https://qa-io.tradelize.com/portal/trader/profile/case4userA";
                        
            TryToSubMirroring(login,password,urlSource);

            Assert.IsTrue(mir.IsElementPresent(By.CssSelector("div[class='modal-dialog']")));
            mir.WaitUntilElementPresent(By.CssSelector("div[class='title']"), 10);
            string actualText = driver.FindElement(By.CssSelector("div[class='title']")).Text;
            Assert.IsTrue(expected == actualText, "Failed actual text is not as expected");
            
        }

        
        [Test]
        public void TryToSubWhenAlreadySub()//User already subscribed on the mirroring, and try to subscribe to other mirroring
        {
            string expected = "ATTENTION";
            string login = "target1";
            string password = "54321";
            string urlSource = "https://qa-io.tradelize.com/portal/trader/profile/case4userA";

            TryToSubMirroring(login, password, urlSource);

            Assert.IsTrue(mir.IsElementPresent(By.CssSelector("div[class='modal-dialog']")));
            loginP.WaitUntilElementPresent(By.CssSelector("div[class='title']"), 10);
            string actualText = driver.FindElement(By.CssSelector("div[class='title']")).Text;
            Assert.IsTrue(expected == actualText, "Failed actual text is not as expected");
        }

        
        [Test]
        public void TryToCreateMirroringWhenAlreadySub() //User already subscribed on the mirroring, and try to create his own mirroring price
        {
            
            string expected = "ATTENTION";
            string login = "target1";
            string password = "54321";

            loginP.SignInByLogin(server, login, password);
            mir.OpenMirroringPage();
            mir.WaitUntilElementPresent(By.CssSelector("a[class='btn-add-offer']"), 10);
            mir.Click(By.CssSelector("a[class='btn-add-offer']"));

            Assert.IsTrue(mir.IsElementPresent(By.CssSelector("div[class='modal-dialog']")));
            loginP.WaitUntilElementPresent(By.CssSelector("div[class='title']"), 10);
            string actualText = driver.FindElement(By.CssSelector("div[class='title']")).Text;
            Assert.IsTrue(expected == actualText, "Failed actual text is not as expected");
        }

        
        [Test]
        public void TryToActivateMirroringPriceWhenSub()//User already subscribed on the mirroring, and try to activate his own mirroring price (Price was created earlier)
        {
           
            string expected = "ATTENTION";
            string login = "case3userA";
            string password = "12345";

            loginP.SignInByLogin(server, login, password);
            mir.OpenMirroringPage();
            if (loginP.IsElementPresent(By.CssSelector("button[class='btn edit']")))
            {
                mir.SelectStatusON();
            }
            else
            {
                mir.CreateNewPlan();
            }
            

            Assert.IsTrue(mir.IsElementPresent(By.CssSelector("div[class='modal-dialog']")));
            loginP.WaitUntilElementPresent(By.CssSelector("div[class='title']"), 10);
            string actualText = driver.FindElement(By.CssSelector("div[class='title']")).Text;
            Assert.IsTrue(expected == actualText, "Failed actual text is not as expected");
        }

        public void TryToSubMirroring(string login,string password,string sourceUrl)
        {
             
            loginP.SignInByLogin(server, login, password);
            loginP.WaitUntilElementPresent(By.ClassName("logo"), 10);
            loginP.GoUrl(sourceUrl); //go to source profile
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(6);
            loginP.Click(By.ClassName("tooltips-mirroring"));
            mir.StartMirroring();
            driver.SwitchTo().ActiveElement();                        
        }
                            


    }

    
}
