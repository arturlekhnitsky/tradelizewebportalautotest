﻿using AventStack.ExtentReports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using TradelizeWebPortal.ExReport;
using TradelizeWebPortal.Properties;
using TradelizeWebPortal.Settings;
using static TradelizeWebPortal.Pages.Base;
using NUnit.Framework;
using TradelizeWebPortal.Objects;

namespace TradelizeWebPortal.Tests
{

    public class DemoModeSwitcherTest : Browsers
    {
        [Test]
        public void SwitchToDemoTest()
        {
            logs = report.CreateTest("Test mode Switcher - turning ON Demo");
            var user = accessP.GetMainUser(server);
            
            loginP.SignInByLogin(server, user.Login, user.Password);
            logs.Log(Status.Info, "LOGIN is successful");
            loginP.ClickSwitchMode();
            logs.Log(Status.Info, "Click to switch mode");

            try
            {
                if (loginP.IsDemoModeActive())
                {
                    logs.Pass("Demo mode is Active. PASSED");
                }
            }
            catch (NoSuchElementException)
            {
                logs.Fail("Demo mode isn't turned ON");
                throw;
            }


            report.Flush();
        }
                
    }
}
