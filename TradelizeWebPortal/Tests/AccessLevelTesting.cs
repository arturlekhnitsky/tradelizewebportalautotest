﻿using AventStack.ExtentReports;
using NUnit.Framework;
using System;
using TradelizeWebPortal.Settings;


namespace TradelizeWebPortal.Tests
{
    public class AccessLevelTesting : Browsers
    {
        [Test]
        public void CheckStatisticAccess()
        {
            int count = 1;
            bool actual;
            bool expected;
            var availableAccess = accessP.GetAccessList();
            var user = accessP.GetMainUser(server);
            var users = accessP.GetTestUsers(server);

            if (user != null)
            {
                foreach (var access in availableAccess)
                {
                    loginP.SignInByLogin(server, user.Login, user.Password);
                    accessP.ChangeStatisticAccess(server, access);
                    loginP.LogOut();

                    foreach (var ro in users)
                    {
                        logs = report.CreateTest($"TC{count}. Check Statistic Access: {access} (for user role: {ro.Role})");
                        loginP.OpenLoginPage(server);
                        expected = accessP.GetExpectedAccess(ro.Role, access);

                        //Checking for each user from the list
                        if (ro.Login != "")
                        {
                            logs.Info($"Check User with Role: {ro.Role.ToUpper()}");
                            loginP.SignInByLogin(server, ro.Login, ro.Password);
                            accessP.GoToTraderProfile(user.Login, server);
                            count++;
                        }
                        else
                        {
                            logs.Log(Status.Info, $"Check User with Role: {ro.Role.ToUpper()}");
                            accessP.GoToTraderProfile(user.Login, server);
                            count++;
                        }
                        actual = accessP.IsStatisticPresent();

                        try
                        {
                            Assert.IsTrue(expected == actual);
                            logs.Pass("As Expected");
                        }
                        catch (AssertionException)
                        {
                            logs.Info($"Statistic visible expected: {expected}, but actual: {actual}");
                            logs.Fail("Fail");
                        }

                        report.Flush();

                        if (loginP.IsHeaderDropDownNenuPresent())
                            loginP.LogOut();
                    }

                }
            }

        }

        
        [Test]
        public void CheckTradeAccess()
        {
            int count = 1;
            var availableAccess = accessP.GetAccessList();
            var mainUser = accessP.GetMainUser(server);
            var users = accessP.GetTestUsers(server);
            bool actual;
            bool expected;

            foreach (var access in availableAccess)
            {
                loginP.SignInByLogin(server, mainUser.Login, mainUser.Password);
                accessP.ChangeTradeAccess(server, access);
                loginP.LogOut();

                foreach (var ro in users)
                {
                    logs = report.CreateTest($"TC{count}. Check Trade Access: {access}");
                    loginP.OpenLoginPage(server);
                    expected = accessP.GetExpectedAccess(ro.Role, access);

                    //Checking for each user from the list
                    logs.Info($"Check User with Role: {ro.Role.ToUpper()}");

                    if (ro.Login != "")
                    {
                        loginP.SignInByLogin(server, ro.Login, ro.Password);
                        accessP.GoToTraderProfile(mainUser.Login, server);
                        accessP.GoToTradesInDiary();
                        count++;
                    }
                    else
                    {
                        accessP.GoToTraderProfile(mainUser.Login, server);
                        accessP.GoToTradesInDiary();
                        count++;
                    }

                    loginP.Sleep(4);
                    actual = accessP.AreTradesInDiaryPresent(); //Check Are Trades Present? 

                    try
                    {
                        Assert.IsTrue(expected == actual);
                        logs.Pass("As Expected");
                    }
                    catch (Exception)
                    {
                        logs.Fail($"Fail - {ro.Login}");
                    }
                    report.Flush();

                    if (loginP.IsHeaderDropDownNenuPresent())
                        loginP.LogOut();
                }

            }
        }

        //Social feed for Guest
        [Test]
        public void TestSocialFeedForGuest()
        {
            int expectedCountOfTab = 1;

            logs = report.CreateTest("Social Feed For Guest");
            logs.Info("Expected: Only Post tab should be visible for Guest");
            loginP.OpenTradelizePortal(server);
            loginP.OpenSocialFeedPage();

            var (actualTabName, actualTabCount) = sfeedP.GetActualTabsOnSocialFeed(); //item1 = name of tabs, item2 = Row Count;

            try
            {
                Assert.IsTrue(expectedCountOfTab == actualTabCount);
                logs.Pass("As Expected");
            }
            catch (AssertionException)
            {
                logs.Fail($"Fail: Actual: for Guest visible {actualTabCount} tabs:{actualTabName}");
            }

            report.Flush();
        }
    }
}
