﻿using NUnit.Framework;
using System;
using TradelizeWebPortal.Settings;

namespace TradelizeWebPortal.Tests
{
    public class Navigate : Browsers
    {
        [Test]
        public void OpenSupportTest()
        {
            bool actual;
            logs = report.CreateTest("Open Support page");
            var user = loginP.GetMainUser(server);
            loginP.SignInByLogin(server, user.Login, user.Password);
            loginP.OpenSupportPage();
            actual = support.IsCreateBtnPresent();

            try
            {
                Assert.IsTrue(actual);
                logs.Pass("Support Page is Open");
            }
            catch (Exception)
            {
                logs.Fail($"Support page is not open");
            }
            report.Flush();
        }

        [Test]
        public void OpenMyProfileTest()
        {
            bool actual;
            logs = report.CreateTest("Open My Profile page");
            var user = loginP.GetMainUser(server);
            loginP.SignInByLogin(server, user.Login, user.Password);
            loginP.OpenMyProfilePage();
            actual = myProf.IsProfileBlockPresent();

            try
            {
                Assert.IsTrue(actual);
                logs.Pass("My Profile page is Open");
            }
            catch (Exception)
            {
                logs.Fail("My Profile page is not open");
                
            }
            report.Flush();
        }

        [Test]
        public void OpenSocialFeedTest()
        {
            bool actual;
            logs = report.CreateTest("Open Social Feed page");
            var user = loginP.GetMainUser(server);
            loginP.SignInByLogin(server, user.Login, user.Password);

            try
            {
                loginP.OpenSocialFeedPage();
                actual = socfeed.IsSocialFeedOpen();
                Assert.IsTrue(actual);
                logs.Pass("Social Feed page is Open");
            }
            catch (Exception)
            {
                logs.Fail("Social Feed page is not open");

            }
            report.Flush();
        }

        [Test]
        public void OpenFundsTest()
        {
            bool actual;
            logs = report.CreateTest("Open Funds page");
            var user = loginP.GetMainUser(server);
            loginP.SignInByLogin(server, user.Login, user.Password);

            try
            {
                funds.OpenFundsPage();
                actual = funds.IsFundsPageOpen();
                Assert.IsTrue(actual);
                logs.Pass("Funds page is Open");
            }
            catch (Exception)
            {
                logs.Fail("Funds page is not open");
            }
            report.Flush();
        }

        [Test]
        public void OpenPortfolioTest()
        {
            bool actual;
            logs = report.CreateTest("Open Porfolio page");
            var user = loginP.GetMainUser(server);
            loginP.SignInByLogin(server, user.Login, user.Password);

            try
            {
                loginP.OpenPorfolioPage();
                actual = portfolioP.IsPortfolioPageOpen();
                Assert.IsTrue(actual);
                logs.Pass("Porfolio page is Open");
            }
            catch (Exception)
            {
                logs.Fail("Portfolio page is not open");
            }
            report.Flush();
        }

        [Test]
        public void OpenStatisticTest()
        {
            bool actual;
            logs = report.CreateTest("Open Statistic page");
            var user = loginP.GetMainUser(server);
            loginP.SignInByLogin(server, user.Login, user.Password);

            try
            {
                loginP.OpenStatisticPage();
                actual = statisticP.IsStatisticPageOpen();
                Assert.IsTrue(actual);
                logs.Pass("Statistic page is Open");
            }
            catch (Exception)
            {
                logs.Fail("Statistic page is not open");
            }
            report.Flush();
        }


    }
}
