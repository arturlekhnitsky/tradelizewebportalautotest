﻿using AventStack.ExtentReports;
using AventStack.ExtentReports.Reporter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace TradelizeWebPortal.ExReport
{
    public class ExtentManager
    {
        public static ExtentHtmlReporter htmlReporter;
        public static ExtentReports extent;

        private ExtentManager()
        {

        }
        

        public static ExtentReports getInstance()
        {
            if (extent == null)
            {
                string reportPath = @"D:\Reports\Report.html";
                htmlReporter = new ExtentHtmlReporter(reportPath);
                extent = new ExtentReports();
                extent.AttachReporter(htmlReporter);
                extent.AddSystemInfo("OS", "Windows 10");
                string host = Dns.GetHostName();
                extent.AddSystemInfo("Host Name", host);

            }
            return extent;
        }
    }
}
