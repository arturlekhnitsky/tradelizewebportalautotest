﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;


namespace TradelizeWebPortal.Helpers
{
    public class HttpHelper
    {

       private static readonly HttpClient client = new HttpClient();

        //user-portal-api/public/auth/sign-in
        
        //PROD
        //static string server = "https://portal.tradelize.com/user-portal-api/public/auth/sign-in";
        //static string mirStatic = "https://portal.tradelize.com/user-portal-api/private/follower/price/get-list?userName=BigBoy&type=MIRRORING";

        //QA
       static string server = "https://qa-portal.tradelize.com/user-portal-api/public/auth/sign-in";
       static string mirStatic = "https://qa-portal.tradelize.com/user-portal-api/private/follower/price/get-list?userName=case1userB&type=MIRRORING";

       static string log = "";
       static string pass = "";         

       public static async Task<string> GetAccessKey(string log, string passSHA256)
        {

            RequestModel model = new RequestModel()
            {
                #region 
                //prod 
                //login = "bigboy",
                //password = "a6e2871751941bda17b41cb97c5457c01e3620e5d5f686f713782ed3c7b07058"

                //QA
                //login = "case1userB"
                //password = "5994471abb01112afcc18159f6cc74b4f511b99806da59b3caf5a9c173cacfc5"
                #endregion

                login = log,
                password = passSHA256
                

            };

            string json = JsonConvert.SerializeObject(model).ToString();
                
                     
            var httpContent = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(server, httpContent); //change the server

            var responseString = await response.Content.ReadAsStringAsync();
            Console.WriteLine(responseString);

            AuthData authData = JsonConvert.DeserializeObject<AuthData>(responseString);
            Console.WriteLine(authData.Data);

            using (StreamWriter file = new StreamWriter(@"D:\WriteLineResponse.txt", true))
            {
                file.WriteLine(responseString);
            }

            return await Task.FromResult(authData.Data);
        }

       public static async Task<string> GetMirroringPlansJson()
        {
            
            string accessKey = await GetAccessKey(log, pass);
            client.DefaultRequestHeaders.Add("Api-Sign", accessKey);

            HttpResponseMessage response = await client.GetAsync(mirStatic);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();

            using (StreamWriter file1 = new StreamWriter(@"D:\WriteLineResponseJson.txt",true))
            {
                file1.Write(responseBody);
            }

            //dynamic RespObj = JsonConvert.DeserializeObject(responseBody);
            return await Task.FromResult(responseBody);
        }
        
       public async Task<Tuple<bool, bool>> GetStatus()
        {
            dynamic periodsObj = JsonConvert.DeserializeObject(await GetMirroringPlansJson());

            bool currentIsActive = false;
            bool nextIsActive = false;

            foreach (var item in periodsObj.data.currentPeriod)
            {
                currentIsActive = item.isActive;
            }


            foreach (var item in periodsObj.data.nextPeriod)
            {
                nextIsActive = item.isActive;
            }

            Console.WriteLine(currentIsActive + "," + nextIsActive);
            return await Task.FromResult(Tuple.Create(currentIsActive, nextIsActive));

        }
                           
       public class AuthData
        {
            public int AccessKeyExpiredTimestamp { get; set; }
            public string Data { get; set; }
            public bool Success { get; set; }
        }

       public class RequestModel
        {
            public string login { get; set; }
            public string password { get; set; }
        }
       
      
    }
}
