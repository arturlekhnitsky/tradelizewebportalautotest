﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using TradelizeWebPortal.DisposableMailServices;
using TradelizeWebPortal.Pages;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Opera;
using AventStack.ExtentReports;
using TradelizeWebPortal.ExReport;

namespace TradelizeWebPortal.Settings
{
    public class Browsers
    {
        public string server = "QA"; // select the server "QA" or "PROD" 
        
        protected IWebDriver driver;
        private static string browser = "Chrome";
       

        [SetUp]
        public void Initialize()
        {
            


            switch (browser)
            {
                case "Chrome":
                    driver = new ChromeDriver();
                    break;
                case "Firefox":
                    driver = new FirefoxDriver();
                    break;
                case "Opera":
                    driver = new OperaDriver();
                    break;
                default:
                    driver = new ChromeDriver();
                    break;
            }

            

            loginP = new Login(driver);
            regP = new Registration(driver);
            yopP = new Yopmail(driver);
            mir = new Mirroring(driver);
            getTerm = new GetTerminal(driver);
            funds = new Funds(driver);
            accessP = new AccessSettings(driver);
            sfeedP = new SocialFeed(driver);
            support = new Support(driver);
            myProf = new MyProfile(driver);
            socfeed = new SocialFeed(driver);
            portfolioP = new Portfolio(driver);
            statisticP = new Statistic(driver);
            report = ExtentManager.getInstance();
           
        }


        public Login loginP;
        public Registration regP;
        public Yopmail yopP;
        public Mirroring mir;
        public GetTerminal getTerm;
        public Funds funds;
        public AccessSettings accessP;
        public SocialFeed sfeedP;
        public Support support;
        public MyProfile myProf;
        public SocialFeed socfeed;
        public Portfolio portfolioP;
        public Statistic statisticP;
        public ExtentTest logs;
        public ExtentReports report;
        

        [TearDown]
        public void Tear()
        {
            driver.Quit();
        }

    }
}
